<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQAsurveyquestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('QA_survey_questions', function (Blueprint $table) {
            $table->id('id');
            $table->integer('survey_id')->nullable();
            $table->integer('question_id')->nullable();
            $table->integer('order')->nullable();
            $table->integer('required')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('QA_survey_questions');
    }
}
