<?php

namespace SoftCenter\Survey\Helper;

use SoftCenter\Survey\Controllers\QuestionController;
use SoftCenter\Survey\Models\QA_survey;

class Survey
{

    public $survey_id;
    public $survey_name;
    public $survey_description;
    public $survey_questions;
    public $survey_categories;


    public function __construct($data = [])
    {
        if (isset($data['id'])) {
            $survey = QA_survey::find($data['id']);
        } elseif (isset($data['name'])) {
            $survey = QA_survey::create([
                'name' => $data['name'],
                'description' => $data['description']
            ]);
        } else {
            return false;
        }
        $this->survey_id = $survey->id;
        $this->survey_name = $survey->name;
        $this->survey_description = $survey->description;

    }


    public function list()
    {
        return QA_survey::with('questions', 'categories')->get()->toArray();
    }

    public function get($id)
    {
        $survey = QA_survey::find($id)->with('questions', 'categories')->get()->toArray();
        return $survey;
    }

    public function question($question_order = null): QuestionController
    {
        return new QuestionController($this, $question_order);
    }

}