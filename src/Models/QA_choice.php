<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class QA_choice extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_choices';

    protected $fillable = ['choice'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'choice'];

    protected $attributes = [];

    public function questions(){
//        return $this->hasManyThrough(QA_questions::class, QA_survey_questions::class, 'id', 'id', 'question_id', 'id');
        return $this->belongsToMany(
            QA_survey_questions::class,
            QA_survey_questions::class,
            'question_id',
            'id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function categories(){
        return $this->morphToMany(QA_category::class, 'categorable', 'QA_categorable', 'categorable_id', 'category_id');
    }

}
