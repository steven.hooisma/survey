<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class QA_survey extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_survey';

    protected $fillable = ['name', 'description', 'deleted_at'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'name', 'created_at', 'updated_at'];

    protected $attributes = [];

    public function questions(){
//        return $this->hasManyThrough(QA_questions::class, QA_survey_questions::class, 'survey_id', 'id', 'id', 'question_id')->orderBy('order');
        return $this->belongsToMany(
            QA_questions::class,
            QA_survey_questions::class,
            'survey_id',
            'question_id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function answer()
    {
        return $this->answers()->orderBy('version', 'desc')->first();
    }

    public function answers(){
//        return $this->hasManyThrough(QA_answers::class, QA_survey_questions::class, 'survey_id', 'id', 'id', 'answer_id');
        return $this->belongsToMany(
            QA_answers::class,
            QA_survey_questions::class,
            'survey_id',
            'id',
            'id',
            'survey_question_id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function categories(){
        return $this->morphToMany(
            QA_category::class,
            'categorable',
            'QA_categorable',
            'categorable_id',
            'category_id'
        )->withPivot('order')->orderBy('QA_categorable.order');
    }



}
