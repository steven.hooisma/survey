<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class QA_categorable extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_categorable';

    protected $fillable = ['categorable_type', 'categorable_id', 'category_id', 'order', 'deleted_at'];


    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

    //   protected $casts = {{castsColumns}};

    protected $attributes = [];


}
