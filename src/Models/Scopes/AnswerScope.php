<?php

namespace SoftCenter\Survey\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class AnswerScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        $builder->where('created_by', auth()->id())->orWhere('updated_by', auth()->id());
    }

}
