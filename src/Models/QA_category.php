<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class QA_category extends Model
{


    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_category';

    protected $fillable = ['name', 'description', 'parent_id'];

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'name'];

    protected $attributes = [];

    public function parent(){
        return $this->hasOne(QA_category::class, 'id', 'parent_id');
    }

    public function children(){
        return $this->hasMany(QA_category::class, 'parent_id', 'id');
    }

    public function surveys(){
        return $this->morphedByMany(QA_survey::class, 'categorable', 'QA_categorable', 'category_id', 'categorable_id')->withPivot('order')->orderBy('QA_categorable.order');
    }
    public function answers(){
        return $this->morphedByMany(QA_answers::class, 'categorable', 'QA_categorable', 'category_id', 'categorable_id')->withPivot('order')->orderBy('QA_categorable.order');
    }
    public function choices(){
        return $this->morphedByMany(QA_choice::class, 'categorable', 'QA_categorable', 'category_id', 'categorable_id')->withPivot('order')->orderBy('QA_categorable.order');
    }
    public function questions(){
        return $this->morphedByMany(QA_questions::class, 'categorable', 'QA_categorable', 'category_id', 'categorable_id')->withPivot('order')->orderBy('QA_categorable.order');
    }

//    public function surveys(){
//        return $this->morphToMany(QA_survey::class, 'QA_categorable');
//    }



}
