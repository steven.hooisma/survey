<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use SoftCenter\Survey\Models\Scopes\AnswerScope;

class QA_answers extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_answers';

    protected $fillable = ['survey_question_id', 'answer', 'version', 'period', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'survey_question_id', 'answer', 'version', 'created_at', 'updated_at'];

    protected $attributes = [];

    public function categories(){
        return $this->morphToMany(QA_category::class, 'categorable', 'QA_categorable', 'categorable_id', 'category_id');
    }

    public function question(){
//        return $this->hasManyThrough(QA_questions::class, QA_survey_questions::class, 'id', 'id', 'question_id', 'id');
        return $this->belongsToMany(
            QA_questions::class,
            QA_survey_questions::class,
            'question_id',
            'id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function survey(){
//        return $this->hasManyThrough(QA_survey::class, QA_survey_questions::class, 'id', 'id', 'survey_id', 'id');
        return $this->belongsToMany(
            QA_survey::class,
            QA_survey_questions::class,
            'survey_id',
            'id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function questionable(){
        return $this->morphTo();
    }

    protected static function boot()
    {

        parent::boot();

        // updating created_by and updated_by when model is created
        static::creating(function ($model) {
            if (!$model->isDirty('created_by')) {
                $model->created_by = auth()->user()->id;
            }
            if (!$model->isDirty('updated_by')) {
                $model->updated_by = auth()->user()->id;
            }
        });

        // updating updated_by when model is updated
        static::updating(function ($model) {
            if (!$model->isDirty('updated_by')) {
                $model->updated_by = auth()->user()->id;
            }
        });


    }

    protected static function booted(): void
    {
        static::addGlobalScope(new AnswerScope());
    }

}
