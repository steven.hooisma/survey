<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class QA_questions extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_questions';

    protected $fillable = ['question', 'question_type', 'deleted_at'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'question', 'question_type', 'created_at', 'updated_at'];

    protected $attributes = [];

    public function surveys(){
//        return $this->hasManyThrough(QA_survey::class, QA_survey_questions::class, 'question_id', 'id', 'id', 'survey_id');
        return $this->belongsToMany(
            QA_survey::class,
            QA_survey_questions::class,
            'question_id',
            'survey_id'
        )->withPivot('order','required')->orderBy('QA_survey_questions.order');
    }

    public function answers(){
//        return $this->hasManyThrough(QA_answers::class, QA_survey_questions::class, 'id', 'id', 'question_id', 'id');
        return $this->belongsToMany(
            QA_answers::class,
            QA_survey_questions::class,
            'question_id',
            'id'
        )->withPivot('order')->orderBy('QA_survey_questions.order');
    }

    public function categories(){
        return $this->morphToMany(QA_category::class, 'categorable', 'QA_categorable', 'categorable_id', 'category_id');
    }

    

}
