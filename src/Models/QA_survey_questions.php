<?php

namespace SoftCenter\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class QA_survey_questions extends Model
{

    use SoftDeletes;
    use HasFactory;

    // connectie die het model gebruikt
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'QA_survey_questions';

    protected $fillable = ['survey_id', 'question_id', 'order', 'required','deleted_at'];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $dates = [];

 //   protected $casts = {{castsColumns}};

    protected $required = ['id', 'created_at', 'updated_at'];

    protected $attributes = [];

    

}
