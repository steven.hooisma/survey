<?php

namespace SoftCenter\Survey\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use SoftCenter\Survey\Models\QA_answers;
use SoftCenter\Survey\Models\QA_questions;
use SoftCenter\Survey\Models\QA_survey;
use SoftCenter\Survey\Models\QA_survey_questions;


class QuestionController
{

    protected $survey;
    protected $id;
    protected $question;

    public function __construct($survey, $question_order = null)
    {
        $this->survey = $survey;
        if ($question_order) {
            $this->question = QA_questions::with(['surveys' => function ($query) {
                $query->where('id', $this->survey->survey_id);
            }, 'answers'])
                ->join('QA_survey_questions', 'QA_questions.id', '=', 'QA_survey_questions.question_id')
                ->where('QA_survey_questions.order', $question_order)
                ->first()
                ->toArray();
        }
    }

    public function list(){
        return QA_questions::where('survey_id', $this->survey->survey_id)->get()->toArray();
    }

    public function add($question, $question_type){
        $question = new QA_questions([
            'question' => $question,
            'question_type' => $question_type
        ]);
        $question->save();

        $order = QA_survey_questions::where('survey_id', $this->survey->survey_id)->orderBy('order', 'desc')->first()->order + 1 ?? 1;
        QA_survey_questions::create([
            'survey_id' => $this->survey->survey_id,
            'question_id' => $question->id,
            'order' => $order
        ]);
        return $question;
    }

    public function get($order){
        return QA_questions::with(['surveys' => function ($query) {
            $query->where('id', $this->survey->survey_id);
        }, 'answers'])
            ->join('QA_survey_questions', 'QA_questions.id', '=', 'QA_survey_questions.question_id')
            ->where('QA_survey_questions.order', $order)
            ->first()
            ->toArray();
    }

//    public function next(){
//        return QA_questions::with(['surveys' => function ($query) {
//            $query->where('id', $this->survey->survey_id);
//        }, 'answers'])
//            ->join('QA_survey_questions', 'QA_questions.id', '=', 'QA_survey_questions.question_id')
//            ->where('QA_survey_questions.order', '>', $order)
//            ->first()
//            ->toArray();
//    }

    public function update($id){

    }

    public function delete($id){

    }

    public function answer($order, $answer){

        $question = QA_questions::with(['surveys' => function ($query) {
//            dd($query);
            $query->where('QA_survey.id', $this->survey->survey_id);
        }, 'answers'])
            ->join('QA_survey_questions', 'QA_questions.id', '=', 'QA_survey_questions.question_id')
            ->where('QA_survey_questions.order', $order)
            ->first()
            ->toArray();

//        dd($question);

        $latestAnswer = QA_answers::where('survey_question_id', $question['id'])
            ->where('questionable_id', Auth::id())
            ->orderBy('version', 'desc')
            ->first();

        $version = $latestAnswer ? $latestAnswer->version + 1 : 1;

        $answer = new QA_answers([
            'answer' => json_encode($answer),
            'survey_question_id' => $question['id'],
            'version' => $version,
        ]);
        $answer->questionable()->associate(User::find(Auth::id()));
        $answer->save();

        return $answer;
    }

    public function answers(){

    }

}
