<?php

namespace Softcenter\Survey;

use Illuminate\Support\ServiceProvider;

class SurveyServiceProvider extends ServiceProvider
{

    public function boot(){
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
    }


    // Register any application services.
    public function register(){

    }

}
